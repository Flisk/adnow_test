CREATE TABLE `desk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL COMMENT 'Имя',
  `content` mediumtext COMMENT 'Настройка мозаики',
  `worktime` int(11) DEFAULT '0' COMMENT 'Время работы (в секундах)',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Таблицы мозаики';