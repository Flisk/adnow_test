<?php

/**
 * Обработчик запросов
 *
 * @author    Alexander Solovev <flisk69@gmail.com>
 */
class Request
{

    /**
     * Параметр из $_GET
     *
     * @static
     * @param $name
     * @param mixed|null $alt
     * @return mixed
     */
    public static function get($name, $alt = null)
    {
        return isset($_GET[$name]) ? $_GET[$name] : $alt;
    }

    /**
     * Параметр из $_POST
     *
     * @static
     * @param $name
     * @param mixed|null $alt
     * @return mixed
     */
    public static function post($name, $alt = null)
    {
        return isset($_POST[$name]) ? $_POST[$name] : $alt;
    }

}