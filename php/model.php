<?php

/**
 * Сохранение доски
 *
 * @author    Alexander Solovev <flisk69@gmail.com>
 */
class Model
{

    /**
     * Сохранение доски
     *
     * @static
     * @param $name
     * @param $data
     * @param $workTime
     * @param bool $force
     * @throws Exception
     */
    public static function saveDesk($name, $data, $workTime, $force = false)
    {
        $connection = Db::getDb();

        $result = $connection->query("SELECT id FROM desk d WHERE d.title = " . $connection->quote($name));
        if($result->rowCount()) {
            // если запись не форсируется, то ошибка 406
            if(!$force) {
                header("HTTP/1.0 406 Not Acceptable");
                exit();
            }
            $row = $result->fetch();

            // обновление данных
            try {
                $connection->beginTransaction();
                $connection->query("UPDATE desk SET `content` = " . $connection->quote(serialize($data)) . ", `worktime` = `worktime` + " . (int)$workTime . ", `updated` = '" . date("Y-m-d H:i:s") . "' WHERE id = " . (int)$row["id"]);
                $connection->commit();
            } catch(Exception $e) {
                header("HTTP/1.0 406 Not Acceptable");
                throw $e;
            }
            print "Доска успешно сохранена";
        } else {
            // добавление записи
            try {
                $connection->beginTransaction();
                $connection->query("INSERT INTO desk SET `title` = " . $connection->quote($name) . ", `content` = " . $connection->quote(serialize($data)) . ", `worktime` = " . (int)$workTime . ", `created` = '" . date("Y-m-d H:i:s") . "'");
                $connection->commit();
            } catch(Exception $e) {
                header("HTTP/1.0 406 Not Acceptable");
                throw $e;
            }
            print "Доска успешно добавлена";
        }
    }

    /**
     * Список досок
     *
     * @static
     */
    public static function getList()
    {
        $connection = Db::getDb();

        $result = $connection->query("SELECT id, title, worktime, (CASE WHEN ISNULL(updated) THEN created ELSE updated END) as modified, created FROM `desk` ORDER BY id DESC");
        if($result->rowCount()) {
            $rows = $result->fetchAll(PDO::FETCH_ASSOC);
        } else {
            $rows = array();
        }
        header("Content-Type: application/json");
        echo json_encode($rows);
        exit();
    }

    /**
     * Данные доски
     *
     * @static
     * @param $id
     */
    public static function loadDesk($id)
    {
        $connection = Db::getDb();

        $result = $connection->query("SELECT * FROM desk WHERE id = " . (int)$id);
        if($result->rowCount()) {
            $row = $result->fetch();
            $deskContent = @unserialize($row["content"]);
            $deskContentResponse = array();
            for($i = 0; $i < DESK_ROWS; $i ++) {
                for($j = 0; $j < DESK_COLS; $j ++) {
                    $deskContentResponse[$i][$j] = $deskContent && isset($deskContent[$i][$j]) ? (int)$deskContent[$i][$j] : DEFAULT_COLOR;
                }
            }
            $data = array(
                "title" => $row["title"],
                "content" => $deskContentResponse,
            );
            header("Content-Type: application/json");
            echo json_encode($data);
            exit();
        } else {
            header("HTTP/1.0 406 Not Acceptable");
            print "Доска не найдена";
            exit();
        }
    }

    /**
     * Удаление доски
     *
     * @static
     * @param $id
     * @throws Exception
     */
    public static function deleteDesk($id)
    {
        $connection = Db::getDb();

        $result = $connection->query("SELECT * FROM desk WHERE id = " . (int)$id);
        if($result->rowCount()) {
            try {
                $connection->beginTransaction();
                $connection->query("DELETE FROM desk WHERE id = " . (int)$id);
                $connection->commit();
            } catch(Exception $e) {
                header("HTTP/1.0 406 Not Acceptable");
                throw $e;
            }
            exit();
        } else {
            header("HTTP/1.0 406 Not Acceptable");
            print "Доска не найдена";
            exit();
        }
    }

}