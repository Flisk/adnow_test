<?php

/**
 * Класс для работы с базой данных
 *
 * @author    Alexander Solovev <flisk69@gmail.com>
 */
class Db
{

    /**
     * pdo
     *
     * @var PDO|null
     */
    protected static $connection = null;

    /**
     * Соединение с базой данных
     *
     * @static
     * @return PDO
     * @throws PDOException
     */
    public static function getDb()
    {
        if(self::$connection !== null) {
            return self::$connection;
        }
        $dbConfig = require "config.php";

        try {
            self::$connection = new PDO("mysql:dbname={$dbConfig["db"]};host={$dbConfig["host"]};charset={$dbConfig["charset"]}", $dbConfig["user"], $dbConfig["password"]);
            self::$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }  catch (PDOException $pdoException) {
            throw $pdoException;
        }
        self::$connection->query("SET NAMES {$dbConfig["charset"]}");

        return self::$connection;
    }

}