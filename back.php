<?php

// content-type
ini_set("default_mimetype", "text/html");
ini_set("default_charset", "UTF-8");

// errors
error_reporting((E_ALL | E_STRICT) & ~E_DEPRECATED);

ini_set("html_errors", false);
ini_set("display_errors", true);
ini_set("display_startup_errors", false);
ini_set("log_errors", true);

// other
setlocale(LC_ALL, "C");
ini_set("date.timezone", "Europe/Moscow");
ini_set("ignore_user_abort", true);
ini_set("default_socket_timeout", 60);

function myAutoload($class)
{
    include "php/" . strtolower($class) . ".php";
}

spl_autoload_register('myAutoload');
define("DESK_ROWS", 20);
define("DESK_COLS", 20);
define("DEFAULT_COLOR", 3);
define("MIN_COLOR", 0);
define("MAX_COLOR", 3);

$action = (string)Request::get("action");
switch($action) {
    case "save":
        $desk = array();
        for($i = 0; $i < DESK_ROWS; $i ++) {
            for($j = 0; $j < DESK_COLS; $j ++) {
                $desk[$i][$j] = (int)Request::post("desk_{$i}_{$j}", DEFAULT_COLOR);
                if($desk[$i][$j] < MIN_COLOR || $desk[$i][$j] > MAX_COLOR) {
                    $desk[$i][$j] = DEFAULT_COLOR;
                }
            }
        }
        Model::saveDesk((string)Request::post("desk-name"), $desk, (int)Request::post("worktime"), (int)Request::post("force"));
        break;

    case "load":
        Model::loadDesk((int)Request::get("id"));
        break;

    case "list":
        Model::getList();
        break;

    case "delete":
        Model::deleteDesk((int)Request::get("id"));
        break;

    default:
        break;
}