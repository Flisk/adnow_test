var formatSecondsToString = function(seconds){
    seconds = parseInt(seconds);
    if(isNaN(seconds))
        seconds = 0;
    var date = new Date(seconds * 1000);
    var hh = date.getUTCHours();
    var mm = date.getUTCMinutes();
    var ss = date.getSeconds();

    if (hh < 10) { hh = "0" + hh; }
    if (mm < 10) { mm = "0" + mm; }
    if (ss < 10) { ss = "0" + ss; }

    return hh + ":" + mm + ":" + ss;
};

jQuery(function($){

    var menu = {
        "draw" : $("#trigger-draw"),
        "load" : $("#trigger-load"),
        "save" : $("#trigger-save")
    };

    var desk = {
        "rows" : 20,
        "cols" : 20,
        "currentDeskName" : "",
        "workTime" : 0,
        "colors" : new Array(
            new Array(221, 0, 0, 1),
            new Array(221, 221, 0, 1),
            new Array(0, 221, 0, 1),
            new Array(255, 255, 255, 0)
        ),
        "cells" : new Array(),
        "field" : $("#field"),
        "init" : function(data){
            if(typeof data != "undefined") {

            }
            desk.field.hide();
            desk.field.delegate(".cell", "click", desk.changeColor);
            setInterval(function(){
                desk.workTime ++;
            }, 1000);
        },
        // сброс доски
        "draw" : function(){
            desk.workTime = 0;
            desk.field.html("").show();
            for(var $i = 0; $i < desk.rows; $i ++) {
                desk.cells[$i] = new Array();
                for(var $j = 0; $j < desk.cols; $j ++) {
                    desk.cells[$i][$j] = $('<div class="cell" data-row="' + $i + '" data-col="' + $j + '" data-color="3">');
                    desk.field.append(desk.cells[$i][$j]);
                }
            }
        },
        "fill" : function(data) {
            for(var $i = 0; $i < desk.rows; $i ++) {
                for(var $j = 0; $j < desk.cols; $j ++) {
                    desk.cells[$i][$j].data("color", data[$i][$j])
                    desk.setColor(desk.cells[$i][$j]);
                }
            }
        },
        // замена цвета у ячейки
        "changeColor" : function(){
            var $this = $(this),
                $color = $this.data("color");

            // выбор индекса цвета
            if($color == (desk.colors.length - 1)) {
                $color = 0;
            } else {
                $color ++;
            }

            $this.data("color", $color);
            desk.setColor($this);
        },
        "setColor" : function($cell){
            var $color = $cell.data("color");
            $cell.css({ "background-color" : "rgba(" + desk.colors[$color][0] + "," + desk.colors[$color][1] + "," + desk.colors[$color][2] + "," + desk.colors[$color][3] + ")" });
        }

    };
    desk.init();

    // "чистая доска"
    menu.draw.bind("click", function(){
        desk.draw();
        menu.save.show();
    });

    // бинд формы
    var saveForm = $("#dialog-save form");
    for(var $i = 0; $i < desk.rows; $i ++) {
        for(var $j = 0; $j < desk.cols; $j ++) {
            saveForm.append('<input name="desk_' + $i + '_' + $j + '" type="hidden" value="3" />');
        }
    }
    $.each($("[data-content='async-form']"), function(i, elm){
        var submitForm = $(elm).find("form"),
            submitButton = submitForm.find("button"),
            answerBlock = $(elm).find("div[data-content='answer']"),
            loading = false;

        // сохранение текущего имени доски для диалога
        desk.currentDeskName = submitForm.find("input[name='desk-name']").val();

        submitForm.ajaxForm({
            "success" : function(answer){
                answerBlock.html('<div class="alert alert-success">' + answer + '</div>');
                loading = false;
                desk.workTime = 0;
                submitButton.removeClass("loading");
            },
            "error" : function(answer){
                loading = false;
                submitButton.removeClass("loading");
                if(answer.status == 406) {
                    if(confirm("Доска с именем «" + submitForm.find("input[name='desk-name']").val() + "» уже существует, переписать?")) {
                        submitForm.find("input[name='force']").val(1);
                        submitButton.trigger("click");
                    }
                } else {
                    answerBlock.html('<div class="alert alert-warning">' + answer.responseText + '</div>');
                }
            }
        });

        // бинд отправки
        submitButton.bind("click.send", function(){
            if(loading)
                return false;

            if($.trim($("#desk-name").val()) == "") {
                answerBlock.html('<div class="alert alert-warning">Введите имя доски</div>');
                return false;
            }

            answerBlock.html("");

            // данные доски
            for(var $i = 0; $i < desk.rows; $i ++) {
                for(var $j = 0; $j < desk.cols; $j ++) {
                    saveForm.find("input[name='desk_" + $i + "_" + $j + "']").val(desk.cells[$i][$j].data("color"));
                }
            }
            saveForm.find("input[name='worktime']").val(desk.workTime);

            loading = true;
            submitButton.addClass("loading");
            submitForm.submit();

            return false;
        });

        submitForm.find("input").keydown(function(event){
            if(event.keyCode == 13) {
                event.preventDefault();
                submitButton.trigger("click");
                return false;
            }
        });

    });

    // открытие диалога сохранения
    $("#dialog-save").on("shown.bs.modal", function(){
        $(this).find("input[name='desk-name']").val(desk.currentDeskName);
        $(this).find("input[name='force']").val(0);
        $(this).find(".alert").html("").hide();
    });

    // загрузка списка
    var descListLoading = $("#desk-list-loading") ,
        deskList = $("#desk-list");

    var loadList = function(){
        descListLoading.show();
        deskList.hide();

        $.ajax({
            "url" : "back.php?action=list",
            "dataType" : "json",
            "success" : function(answerList){
                if(answerList.length == 0) {
                    deskList.html('<p class="big-caption">Не найдено сохраненных досок.</p>');
                } else {
                    var html = "";
                    html += '<table class="table table-bordered table-striped">';
                    html += '<thead>';
                    html += '<th width="40%">Название доски</th>';
                    html += '<th width="15%">Создана</th>';
                    html += '<th width="15%">Модифицирована</th>';
                    html += '<th width="20%">Время работы</th>';
                    html += '<th width="10%">&nbsp;</th>';
                    html += '</thead>';
                    html += '<tbody>';
                    $.each(answerList, function(i, elmList){
                        html += '<tr>';
                        html += '<td>' + elmList.title + '</td>';
                        html += '<td>' + elmList.created + '</td>';
                        html += '<td>' + elmList.modified + '</td>';
                        html += '<td>' + formatSecondsToString(elmList.worktime) + '</td>';
                        html += '<td>';
                        html += '<a data-trigger="load-desk" href="/back.php?action=load&amp;id=' + elmList.id + '"><span class="glyphicon glyphicon-arrow-down"></span></a> ';
                        html += '<a data-trigger="delete-desk" href="/back.php?action=delete&amp;id=' + elmList.id + '"><span class="glyphicon glyphicon-trash"></span></a>';
                        html += '</td>'
                        html += '</tr>';
                    });
                    html += '</tbody>';
                    html += '</table>';
                    deskList.html(html);
                }
                descListLoading.hide();
                deskList.show();
            },
            "error" : function(){
                deskList.html('<div class="alert alert-warning">Ошибка загрузки списка</div>');
            }
        });
    };

    // загрузка сохраненной доски
    deskList.delegate("a[data-trigger='load-desk']", "click", function(){
        descListLoading.show();
        deskList.hide();

        $.ajax({
            "url" : $(this).attr("href"),
            "success" : function(answer){
                menu.draw.click();
                desk.fill(answer.content);
                desk.workTime = 0;
                desk.currentDeskName = answer.title;
                descListLoading.hide();
                deskList.show();
                $("#dialog-list").modal("toggle");
            },
            "error" : function(answer){
                deskList.prepend('<div class="alert alert-warning">' + answer.responseText + '</div>');
                descListLoading.hide();
                deskList.show();
            }
        });

        return false;
    });

    // удаление доски
    deskList.delegate("a[data-trigger='delete-desk']", "click", function(){

        if(!confirm("Удалить доску?")) {
            return false;
        }

        descListLoading.show();
        deskList.hide();

        $.ajax({
            "url" : $(this).attr("href"),
            "success" : function(answer){
                loadList();
            },
            "error" : function(answer){
                deskList.prepend('<div class="alert alert-warning">' + answer.responseText + '</div>');
                descListLoading.hide();
                deskList.show();
            }
        });

        return false;
    });

    // открытие диалога списка
    $("#dialog-list").on("shown.bs.modal", function(){
        loadList();
    });

});